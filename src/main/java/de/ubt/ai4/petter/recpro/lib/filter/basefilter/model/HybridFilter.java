package de.ubt.ai4.petter.recpro.lib.filter.basefilter.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class HybridFilter extends Filter {
    private List<Filter> filters = new ArrayList<>();

    /*

    TODO:
    - Filter positions
    - type of hybridization: der reihe nach / kombinieren?

     */
}
