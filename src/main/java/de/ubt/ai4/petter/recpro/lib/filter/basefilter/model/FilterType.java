package de.ubt.ai4.petter.recpro.lib.filter.basefilter.model;

public enum FilterType {
    COLLABORATIVE,
    KNOWLEDGE_BASED,
    CONTENT_BASED,
    HYBRID,
    BASE,
    PROCESS_AWARE,
    MAIN
}
