package de.ubt.ai4.petter.recpro.lib.filter.basefilter.model;

import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproAttribute;
import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.BpmElement;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class KnowledgeBasedFilter extends Filter {
    private List<BpmElement> bpmElements = new ArrayList<>();
    private List<RecproAttribute> attributes = new ArrayList<>();
    private boolean allInputElements = false;
    private boolean allInputAttributes = false;
}
