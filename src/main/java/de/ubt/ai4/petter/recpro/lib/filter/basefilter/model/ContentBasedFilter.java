package de.ubt.ai4.petter.recpro.lib.filter.basefilter.model;

import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproAttribute;
import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.BpmElement;
import de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model.Rating;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ContentBasedFilter extends Filter {
    private List<BpmElement> bpmElements = new ArrayList<>();
    private List<RecproAttribute> attributes = new ArrayList<>();
    private List<Rating> ratings = new ArrayList<>();

    private boolean allInputElements = false;
    private boolean allInputAttributes = false;
    private boolean allInputRatings = false;
}
