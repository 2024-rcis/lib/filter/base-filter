package de.ubt.ai4.petter.recpro.lib.filter.basefilter.model;

public enum TasklistOrder {
    CREATE_DATE, DUE_DATE, PRIORITY, LIKELINESS
}
