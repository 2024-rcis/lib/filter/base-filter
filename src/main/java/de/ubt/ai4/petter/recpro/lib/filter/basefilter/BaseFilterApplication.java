package de.ubt.ai4.petter.recpro.lib.filter.basefilter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaseFilterApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaseFilterApplication.class, args);
	}

}
